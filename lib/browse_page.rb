class BrowsePage
  include PageObject

  page_url 'http://www1.macys.com/shop/womens-clothing/womens-jeans?id=3111&edge=hybrid&cm_sp=us_hdr-_-women-_-3111_jeans'

  image(:first_product_image, class: 'productThumbnail')

  button(:current_quickview_button, id: "quickViewLauncher")

  div(:quickview_box, id: 'quickView_v2')

  div(:product_name, id: 'quickViewProductName')

  div(:product_price, id: 'quickViewPrices')

  div(:product_overview, id: 'singleProduct')

  div(:product_web_id, id: 'qvProductId')

  div(:product_picture, id: 'previewImage_popupViewer_2')

  span(:quickview_box_close, class: 'container-close', index: 2)

  select_list(:select_size, id: 'quickviewSizeSwatch_Drop')

  image(:add_to_bag_button, id: 'quickviewATBIDImage')

  image(:add_to_wishlist_button, id: 'quickviewATWIDImage')

  div(:error_box, class: 'validationBody')

  div(:successfully_added_to_bag, class: 'qvBagItemTotal')

  div(:error_message, id: 'unavailabilityPanelATB')

  def quickview_button
    self.first_product_element.find_element(:class, "quickViewLauncher")
  end
  
end