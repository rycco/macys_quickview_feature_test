Feature: Product Quickview Box
  In order to get more details about a specific product when browsing at some category browse page
  As a Macys user
  I want to be able to have a quick view of any product 
  
  Background:
    Given I am browsing the Women Jeans Browse Page

  Scenario: Display quickview button
    When I hover the first product's image
    Then I should see the quickview button

  Scenario: Display quickview box
    Given I hover the first product's image
    When I click at its quickview button
    Then I should see the quickview box of that product

  Scenario: Close the box by clicking on the X
    Given I click to quickview the first product
    When I click at the X button on the top right of the box
    Then I should not see the quickview box anymore