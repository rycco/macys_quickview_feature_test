require 'selenium-webdriver'
require 'pry'
require 'world_helper'
require 'page-object'
require 'browse_page'

browser = Selenium::WebDriver.for :chrome
browser.manage.timeouts.implicit_wait = 6
browser.manage.timeouts.script_timeout = 20
browser.manage.timeouts.page_load = 15

@browser = browser
@wait = Selenium::WebDriver::Wait.new(:timeout => 10)

World(WorldHelper)

Before do
  @browser = browser
  @browser.manage.delete_all_cookies
  @browser.navigate.to("about:blank")
end

After do |scenario|
  if scenario.failed?
    # binding.pry
  end
end

at_exit do
  begin
    @browser.quit if @browser
    rescue Errno::ECONNREFUSED
    # Browser must have already gone
    ensure
    @browser = nil
  end
end

VALID_PRODUCT_IDS = %w( 1666753 1717305 1345138 1719422 841315 840794 1333572 1398495 1398494 841355 641087 840505 839074 770372 773352 770395 744481 806119 521172 855560 834696 181718 1012874 1326211 641044 770373 770370 770375 1624005 835609 1624009 826745 770374 1620752 1521058 1521060 1630556 1624011 )
VALID_CATEGORY_IDS = %w( 7495 118 1 )