Feature: Product Quickview Elements
  In order to get more details about a specific product when browsing at some category browse page
  As a Macys user
  I want to be able to see the name, web ID, price and overview of the product on the quickview box 
  
  Scenario Outline: Display element on Quickview Box
    Given I am browsing the Women Jeans Browse Page
    When I click to quickview the first product
    Then I should see the <Element> of the product
    Examples:
      | Element |
      | name |
      | ID |
      | price |
      | overview |