Feature: Product Quickview Add To Bag
  In order to get more details about a specific product when browsing at some category browse page
  As a Macys user
  I want to be able to have a quick view of any product
  And I want to be able to add the product to the bag
  
  Background:
    Given I am browsing the Women Jeans Browse Page
    And I click to quickview the first product

  Scenario: Add to Bag
    Given I have choose a size for the product
    When I click at the Add to Bag button
    Then I should see the message confirming that it was added successfully

  Scenario: Add to Bag without selecting the size
    When I click at the Add to Bag button
    Then I should see an error message asking for select the size
