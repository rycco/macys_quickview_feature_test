Transform /^(-?\d+)$/ do |number|
  number.to_i
end

When(/^I hover the first product's image$/) do
  @page.first_product_image_element.hover
end

Then(/^I should see the quickview button$/) do
  expect(@page.current_quickview_button?).to be_equal(true)
end

Then(/^I should see the quickview box of that product$/) do
  expect(@page.quickview_box?).to be_equal(true)
end

Given(/^I am browsing the Women Jeans Browse Page$/) do
  @page = BrowsePage.new(@browser)
  @page.goto
  @page.wait_until do
     @browser.execute_script("return document.readyState") == "complete"
  end
end

When(/^I click at its quickview button$/) do
  @page.current_quickview_button
end

When(/^I click to quickview the first product$/) do
  step "I hover the first product's image"
  step "I click at its quickview button"
end
