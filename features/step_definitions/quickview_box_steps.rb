Then(/^I should see the name of the product$/) do
  expect(@page.product_name?).to be_equal(true)
end

Then(/^I should see the ID of the product$/) do
  expect(@page.product_web_id?).to be_equal(true)
end

Then(/^I should see the price of the product$/) do
  expect(@page.product_price?).to be_equal(true)
end

Then(/^I should see the overview of the product$/) do
  expect(@page.product_overview?).to be_equal(true)
end

When(/^I click at the X button on the top right of the box$/) do
   @page.quickview_box_close_element.click
end

Then(/^I should not see the quickview box anymore$/) do
  expect(@page.quickview_box?).to be_equal(false)
end

Given(/^I have choose a size for the product$/) do
  @page.select_size_element.select_value('6')
end

When(/^I click at the Add to Bag button$/) do
  # binding.pry
  @page.add_to_bag_button_element.click
end

Then(/^I should see the message confirming that it was added successfully$/) do
  expect(@page.successfully_added_to_bag?).to be_equal(true)
end

Then(/^I should see an error message asking for select the size$/) do
  expect(@page.error_message?).to be_equal(true)
end